using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class UserServices : IUserService
    {
        private readonly WarehouseContext context;

        public UserServices(WarehouseContext context)
        {
            this.context = context ??
                throw new ArgumentNullException(nameof(context));
        }

        public User GetUser(string firstName, string lastName, string companyName)
        {
            var company = this.context.Companies
                .FirstOrDefault(c => c.Name == companyName);

            var userName = firstName.ElementAt(1) + lastName + "@" + company.Name.Replace(" ", "");
            //var userName = firstName.Take(1) + lastName + "@" + company.Name.Replace(" ", "");
            var user = this.context.Users
                .FirstOrDefault(u => u.Username == userName);
            if (user != null && user.IsDeleted == false)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public User CreateUser(string firstName, string lastName, string roleName, 
            string companyName)
        {
            var company = this.context.Companies.FirstOrDefault(c => c.Name == companyName);
            if (company == null)
            {
                throw new ArgumentException($"Comapny {companyName} does not exist");
            }
            string username = firstName.ElementAt(1) + lastName + "@" + company.Name.Replace(" ", "");
            //if (this.context.Users.Any(u => u.FirstName == firstName))
            //{
            //    throw new ArgumentException($"User {username} already exists");
            //}
            var role = this.context.Roles.FirstOrDefault(b => b.Name == roleName);
            if (role == null)
            {
                throw new ArgumentException($"Role {roleName} does not exist");
            }
           
            var user = new User()
            {
                FirstName = firstName,
                LastName = lastName,
                Username = username,
                Password = lastName + firstName.Count(),
                Role = role,
                Company = company
            };
            this.context.Users.Add(user);
            this.context.SaveChanges();
            return user;
        }

        public User ModifyPassword(string password, User user)
        {
            user.Password = password;
            user.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return user;
        }

        public User ModifyRole(string roleName, User user)
        {
            var role = this.context.Roles.FirstOrDefault(r => r.Name == roleName);            
            if (role == null)
            {
                throw new ArgumentException($"Role {roleName} does not exist");
            }
            user.Role.Users.Remove(user);
            user.RoleID = role.Id;
            user.Role = role;
            role.Users.Add(user);
            this.context.SaveChanges();
            return user;
        }
        public List<User> ListAllUsers()
        {
            var temp_users = this.context.Users.ToList();
            List<User> users = new List<User>();
            foreach (var user in temp_users)
            {
                if (user.IsDeleted == false)
                {
                    users.Add(user);
                }
            }
            return users;

        }

        public User RemoveUser(User user)
        {
            user.IsDeleted = true;
            user.DeletedOn = DateTime.Now;
            this.context.SaveChanges();
            return user;
        }

        public User RetrieveUser(string username)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Username == username);
            return user;
        }

        public Role GetRole(string roleName)
        {
            var role = this.context.Roles
                .FirstOrDefault(b => b.Name.ToLower() == roleName.ToLower());

            if (role != null && role.IsDeleted == false)
            {
                return role;
            }
            else
            {
                return null;
            }
        }

        public Role CreateRole(string roleName)
        {
            var role = GetRole(roleName);
            if (role == null)
            {
                var newRole = new Role()
                {
                    Name = roleName,
                };
                this.context.Roles.Add(role);
                this.context.SaveChanges();
                return role;
            }
            else
            {
                return null;
            }
        }

        public List<string> GetUserPermissions(User user)
        {
            var roleID = user.RoleID;
            var permissions = this.context.RolePermissions.Where(r => r.RoleID == roleID).Include(x => x.Permission).Select(z => z.Permission.Name).ToList();
            return permissions;
        }
    }
}
