﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class PermissionServices : IPermissionService
    {
        private readonly WarehouseContext context;

        public PermissionServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Permission CreatePermission(string name)
        {
            if (this.context.Permissions.Any(p => p.Name == name))
            {
                throw new ArgumentException("Permission already exists.");
            }
            var permission = new Permission()
            {
                Name = name
            };
            this.context.Permissions.Add(permission);
            this.context.SaveChanges();
            return permission;
        }
    }
}
