﻿using System;
using System.Linq;
using System.Text;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class ProductServices : IProductService

    {
        private readonly WarehouseContext context;

        public ProductServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Product GetProduct(string productName)
        {
            var product = this.context.Products
                .FirstOrDefault(p => p.Name.ToLower() == productName.ToLower());

            if (product != null && product.IsDeleted == false)
            {
                return product;
            }
            else
            {
                return null;
            }
        }

        public Product AddProduct(string productName, string brandName, string supplierName, string categoryName,
            decimal price, int onStock)
        {
            var product = GetProduct(productName);
            if (product != null && product.IsDeleted == false)
            {
                return null;
            }
            else
            {
                var brand = this.context.Brands
                    .FirstOrDefault(b => b.Name == brandName);
                if (brand == null)
                {
                    throw new ArgumentNullException($"Brand {brandName} not found.");
                }
                var supplier = this.context.Suppliers
                    .FirstOrDefault(s => s.Name == supplierName);
                if (supplier == null)
                {
                    throw new ArgumentNullException($"Supplier {supplierName} not found.");
                }
                var category = this.context.Categories
                    .FirstOrDefault(c => c.Name == categoryName);
                if (category == null)
                {
                    throw new ArgumentNullException($"Category {categoryName} not found.");
                }
                var newProduct = new Product()
                {
                    CreatedOn = DateTime.Now,
                    Name = productName,
                    Brand = brand,
                    Price = price,
                    OnStock = onStock,
                    Supplier = supplier,
                    Category = category

                };
                this.context.Products.Add(newProduct);
                this.context.SaveChanges();
                return newProduct;
            }
        }

        public string GetAllProductInformation(Product product)
        {
                var brand = this.context.Brands
                .FirstOrDefault(b => b.Id == product.BrandID);
                var supplier = this.context.Suppliers
                .FirstOrDefault(s => s.Id == product.SupplierID);
                var category = this.context.Categories
               .FirstOrDefault(c => c.Id == product.CategoryID);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Description of the product {product.Name}:");
                sb.AppendLine($"Id: {product.Id.ToString()}");
                sb.AppendLine($"Name: {product.Name}");
                sb.AppendLine($"The product was created on: {product.CreatedOn.ToString()}");
                sb.AppendLine($"Brand: {brand.Name}");
                sb.AppendLine($"Supplier: {supplier.Name}");
                sb.AppendLine($"Category: {category.Name}");
                sb.AppendLine($"Price: {product.Price.ToString()}");
                sb.AppendLine($"Current quantity of the: {product.OnStock.ToString()}");
                return sb.ToString();
        }

        public Product ChangeBrand(Product product, string newBrandName)
        {
            var newBrand = this.context.Brands
                .FirstOrDefault(b => b.Name == newBrandName);
            if (newBrand == null)
            {
                return null;
            }
            product.BrandID = newBrand.Id;
            product.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }
        public Product ChangeSupplier(Product product, string newSupplierName)
        {
            var newSupplier = this.context.Suppliers
                .FirstOrDefault(b => b.Name == newSupplierName);
            if (newSupplier == null)
            {
                return null;
            }
            product.SupplierID = newSupplier.Id;
            product.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }
        public Product ChangeCategory(Product product, string newCategoryName)
        {
            var newCategory = this.context.Categories
                .FirstOrDefault(b => b.Name == newCategoryName);
            if (newCategory == null)
            {
                throw new ArgumentException($"There is no registred category with name {newCategoryName}");
            }
            product.CategoryID = newCategory.Id;
            product.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }
        public Product ChangeName(Product product, string newName)
        {
            product.Name = newName;
            product.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }
        public Product ChangePrice(Product product, decimal newPrice)
        {
            product.Price = newPrice;
            product.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }

        public Product RemoveProduct(Product product)
        { 
            product.IsDeleted = true;
            product.DeletedOn = DateTime.Now;
            this.context.SaveChanges();
            return product;
        }

        public int GetProductQuantity (string productName)
        {
            int quantity = 0;
            var product = this.context.Products
                .FirstOrDefault(p => p.Name.ToLower() == productName.ToLower());
            if (product == null || product.IsDeleted == false)
            {
                quantity = product.OnStock;
                return quantity;
            }
            else
            {
                return -1;
            }
        }
    }
}