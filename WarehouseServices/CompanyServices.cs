﻿using System;
using System.Linq;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class CompanyServices : ICompanyService
    {
        private readonly WarehouseContext context;

        public CompanyServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Company GetCompany(string companyName)
        {
            var company = this.context.Companies
                .FirstOrDefault(os => os.Name == companyName);
            if (company != null && company.IsDeleted == false)
            {
                return company;
            }
            else
            {
                return null;
            }
        }

        public Company CreateCompany(string companyName, string adress, string EIK,
            int discount)
        {
            var company = GetCompany(companyName);
            if (company != null)
            {
                return null;
            }
            else
            {
            var newCompany = new Company()
            {
                Name = companyName,
                Address = adress,
                EIK = EIK,
                Discount = discount
                
            };
            this.context.Companies.Add(newCompany);
            this.context.SaveChanges();
            return newCompany;
            }
        }
    }
}
