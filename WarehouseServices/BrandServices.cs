﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class BrandServices : IBrandService
    {
        private readonly WarehouseContext context;

        public BrandServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Brand GetBrand(string brandName)
        {
            var brand = this.context.Brands
                .FirstOrDefault(b => b.Name.ToLower() == brandName.ToLower());

            if (brand != null && brand.IsDeleted == false)
            {
                return brand;
            }
            else
            {
                return null;
            }
        }

        public Brand AddNewBrand(string brandName)
        {
            var brand = GetBrand(brandName);
            if (brand != null)
            {
                return null;
            }
            else
            {
                var newBrand = new Brand()
                {
                    Name = brandName,
                };
                this.context.Brands.Add(newBrand);
                this.context.SaveChanges();
                return newBrand;
            }
        }

        public List<string> GetAllBrandProducts(string brandName)
        {
            List<string> products = new List<string>();
            var brand = GetBrand(brandName);
            if (brand != null)
            {
                brand = this.context.Brands
                    .Include(b => b.Products)
                .FirstOrDefault(b => b.Name.ToLower() == brandName.ToLower());
                var temp_products = brand.Products.ToList();
                foreach (var product in temp_products)
                {
                    string temp = $"Product name: {product.Name}, brand: {brandName}, price: {product.Price.ToString()}.";
                    products.Add(product.Name);
                }
                return products;
            }
            else
            {
                return null;
            }
        }
    }
}
