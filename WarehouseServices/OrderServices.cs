﻿using System;
using Warehouse.Data;
using System.Linq;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace WarehouseServices
{
    public class OrderServices : IOrderService
    {
        private readonly WarehouseContext context;

        public OrderServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public OrderStatus GetStatus(string statusName)
        {
            var status = this.context.OrderStatus
                .FirstOrDefault(os => os.Name == statusName);
            if (status != null && status.IsDeleted == false)
            {
                return status;
            }
            else
            {
                return null;
            }
        }

        public OrderStatus CreateStatus(string statusName)
        {
            var status = GetStatus(statusName);
            if (status == null)
            {
                var newStatus = new OrderStatus()
                {
                    Name = statusName,
                };
                this.context.OrderStatus.Add(newStatus);
                this.context.SaveChanges();
                return newStatus;
            }
            else
            {
                return null;
            }
        }

        public Order CreateOrder(string status, int userID)
        {
            var statusID = this.context.OrderStatus.FirstOrDefault(x => x.Name == status).Id;
            var newOrder = new Order()
            {
                StatusID = statusID,
                UserID = userID
            };
            this.context.Orders.Add(newOrder);
            this.context.SaveChanges();
            return newOrder;
        }

        public string AddPurchase(int orderID, int productID, int quantity)
        {
            try
            {
                var newPurchase = new Purchase()
                {
                    OrderID = orderID,
                    ProductID = productID,
                    Quantity = quantity
                };
                this.context.Purchases.Add(newPurchase);
                this.context.SaveChanges();
                return "Purchase created.";
            }
            catch { return "Purchase failed."; }
        }

        public string ListAllOrders(string allOrders)
        {
            var orders = this.context.Orders
                .Include(o => o.Creator.Orders)
                .Include(o => o.Status.Orders)
                    .ToList();
            StringBuilder sb = new StringBuilder();
            //List<string> orderResult = new List<string>();
            if (orders.Count <= 0)
            {
                return "";
            }
            else
            {
            foreach (var order in orders)
            {
                string temp = $"Order with id {order.Id} was created on {order.CreatedOn}" +
                    $" from user {order.Creator.FirstName} {order.Creator.LastName}";
                sb.AppendLine(temp);
            }
            return sb.ToString();
            }
            
        }

    }
}