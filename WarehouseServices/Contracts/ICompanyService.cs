﻿using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface ICompanyService
    {
        Company GetCompany(string companyName);
        Company CreateCompany(string companyName, string adress, string EIK,
            int discount);
    }
}
