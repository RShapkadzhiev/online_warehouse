﻿using System.Collections.Generic;
using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface IUserService
    {
        User CreateUser(string firstName, string lastName, string roleName, string companyName);
        User ModifyPassword(string password, User user);
        User ModifyRole(string roleName, User user);
        User RemoveUser(User user);
        User RetrieveUser(string username);
        Role CreateRole(string roleName);
        Role GetRole(string roleName);
        List<string> GetUserPermissions(User user);
        User GetUser(string firstName, string lastName, string companyName);
        List<User> ListAllUsers();
    }
}
