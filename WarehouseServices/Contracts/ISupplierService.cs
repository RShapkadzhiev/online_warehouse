﻿using System.Collections.Generic;
using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface ISupplierService
    {
        Supplier AddSupplier(string supplierName);
        Supplier GetSupplier(string supplierName);
        List<string> GetAllProducts(string supplierName);
        Supplier ModifyName(Supplier supplier, string newSupplierName);
        Supplier RemoveSupplier(Supplier supplier);
    }
}
