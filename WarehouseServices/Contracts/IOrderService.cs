﻿using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface IOrderService
    {
        OrderStatus GetStatus(string statusName);
        OrderStatus CreateStatus(string statusName);
        Order CreateOrder(string status, int userID);
        string AddPurchase(int orderID, int productID, int quantity);
        string ListAllOrders(string allOrders);
    }
}
