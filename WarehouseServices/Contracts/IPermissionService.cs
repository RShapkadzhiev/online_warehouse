﻿using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface IPermissionService
    {
        Permission CreatePermission(string name);
    }
}
