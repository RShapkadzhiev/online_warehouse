﻿using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface ICategoryService
    {
        Category GetCategory(string categoryName);
        Category CreateCategory(string name, Category upperCategory);
    }
}
