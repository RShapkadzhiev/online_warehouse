﻿using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface IProductService
    {
        Product AddProduct(string productName, string brandName, string supplierName,
            string categoryName, decimal price, int onStock);
        Product GetProduct(string productName);
        string GetAllProductInformation(Product product);
        Product ChangeBrand(Product product, string newBrandName);
        Product ChangeSupplier(Product product, string newSupplierName);
        Product ChangeCategory(Product product, string newCategoryName);
        Product ChangeName(Product product, string newName);
        Product ChangePrice(Product product, decimal newPrice);
        int GetProductQuantity(string productName);
        Product RemoveProduct(Product product);
    }
}
