﻿using System.Collections.Generic;
using Warehouse.Data.Models;

namespace WarehouseServices.Contracts
{
    public interface IBrandService
    {
        Brand GetBrand(string brandName);
        Brand AddNewBrand(string brandName);
        List<string> GetAllBrandProducts(string brandName);
    }
}
