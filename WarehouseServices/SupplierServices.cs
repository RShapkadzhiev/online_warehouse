﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class SupplierServices : ISupplierService
    {

        private readonly WarehouseContext context;

        public SupplierServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Supplier GetSupplier(string supplierName)
        {
            var supplier = this.context.Suppliers
                .FirstOrDefault(s => s.Name.ToLower() == supplierName.ToLower());

            if (supplier != null && supplier.IsDeleted == true)
            {
                return null;
            }
            else
            {
                return supplier;
            }
        }

        public Supplier AddSupplier(string supplierName)
        {
            var supplier = GetSupplier(supplierName);
            if (supplier != null && supplier.IsDeleted == false)
            {
                return null;
            }
            else
            {
                var newSupplier = new Supplier()
                {
                    Name = supplierName,
                };
                this.context.Suppliers.Add(newSupplier);
                this.context.SaveChanges();
                return newSupplier;
            }
        }

        public List<string> GetAllProducts(string supplierName)
        {
            List<string> products = new List<string>();
            var supplier = GetSupplier(supplierName);
            if (supplier != null)
            {
                supplier = this.context.Suppliers
                    .Include(b => b.Products)
                .FirstOrDefault(b => b.Name.ToLower() == supplierName.ToLower());
                var temp_products = supplier.Products.ToList();
                foreach (var product in temp_products)
                {
                    products.Add(product.Name);
                }
                return products;
            }
            else
            {
                return null;
            }
        }

        public Supplier ModifyName(Supplier supplier, string newSupplierName)
        {
            supplier.Name = newSupplierName;
            supplier.ModifiedOn = DateTime.Now;
            this.context.SaveChanges();
            return supplier;
        }

        public Supplier RemoveSupplier(Supplier supplier)
        {
            supplier.IsDeleted = true;
            supplier.DeletedOn = DateTime.Now;
            this.context.SaveChanges();
            return supplier;
        }
    }
}
