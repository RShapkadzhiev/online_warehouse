﻿using System;
using System.Linq;
using Warehouse.Data;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;

namespace WarehouseServices
{
    public class CategoryServices : ICategoryService
    {
        private readonly WarehouseContext context;

        public CategoryServices(WarehouseContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Category GetCategory(string categoryName)
        {
            var category = this.context.Categories
                .FirstOrDefault(c => c.Name == categoryName);
            if (category != null && category.IsDeleted == false)
            {
                return category;
            }
            else
            {
                return null;
            }
        }
        
        public Category CreateCategory(string name, Category upperCategory)
        {
            var category = GetCategory(name);
            if (category == null)
            {
                category = new Category()
                {
                    Name = name,
                    Parent = upperCategory,
                };
                this.context.Add(category);
                this.context.SaveChanges();
                return category;
            }
            else 
            {
                return null;
            }
        }
    }
}
