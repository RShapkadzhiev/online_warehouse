# Online Warehouse

## The Team
We are ***Zornitsa Filipova*** and ***Rusi Shapkadzhiev*** (ladies first approach applied here).
Team name is still under development :-)  
No teamleader.  
We use this [Trello Board](https://trello.com/b/cWBt73He) to organise our project tasks and track their progress.

## The Project
### Overview
This is a CLI Warehouse Application, that supports multiple user roles. The App owners are able to administer customer accounts, process orders, manage the products and all related data.
The customers are allowed to create, modify, submit and list their orders and get invoices once the order is comleted.

The solution is divided in three projects, as follows:
- Data
- Services
- UI

Each project represents a layer from the layered architecture pattern the solution is based upon.
### Data
This layer handles the Database connectivity i.e. data entities, data connections, etc.  
The Data layer is responsible for exposing the data stored in the MS SQL Server 2017 database to the Services layer. Entity Framework Code First approach is used.

*Warehouse Database Diagram*

![Database Diagram](Warehouse_Diagram.png)
### Services
This layer holds the basic CRUD operations, that are necessary to cover all the business functionality. They are split into several classes, each class holding the relevant CRUD operations for a specific entity.
The list of service classes includes (but is not limited to) :
- UserServices
- OrderServices
- ProductServices
- CategoryServices
- SupplierServices
- CompanyServices

### UI
This layer contains the CLI commands, that the user can execute depending on the user role.
Each command uses one or more service classes from the Services layer.
The list of commands includes (but is not limited to) :
- login
- logout
- addcategory 
- deletecategory
- addproduct
- modifyproduct
- removeproduct
- importproducts
- createorder
- vieworderstatus
- getinvoice
- showmylastorder
- modifyorder
- submitorder
- createaccount
- modifyaccount
- deleteaccount
- createcompany
- modifycompany
- adddiscount
- listorders
- changeorderstatus
- listproductquantities
- generatestockorder
- listorderreport (daily/weekly/monthly)
- listtopclients (top X clients for period Y)
- createsupplier
- removesupplier
- modifysupplier

---

