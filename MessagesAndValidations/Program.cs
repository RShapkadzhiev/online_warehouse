﻿using Autofac;
using WarehouseUI.Core;

namespace MessagesAndValidations
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<WarehouseModule>();
            var container = builder.Build();

            var engine = container.Resolve<Engine>();
            engine.Start();
        }
    }
}
