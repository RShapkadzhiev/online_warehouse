﻿namespace WarehouseUI.Core.Messages
{
    public static class MessagesClass
    {
        #region System messages 
        public static string introducingMessage = 
            "Hello! You are using our warehouse management program. Please follow the instructions " +
            "below.\n" + "If you want to see all provided from the program services, please press" +
            " \"enter\" or write\n \"Show all services\"\n" + "If you want to exit the program please" +
            " write \"exit\"";
        public static string invalidIndexOfServiceMessage =
            "You entered invalid service index. Please enter correct index.";
        public static string invalidServiceMessage = "Invalid service.";
        public static string filterMessage = "You can filter all services and use only those which you need." +
            " In order to do so, please press \"#\" or Filter services and then the filter word.";
        #endregion

        #region Command messages
        public static string invalidProductNameMessage = "Please enter correct product name or" +
            " \"exit\" to stop executing of the command.";
        public static string invalidBrandNameMessage = "Please enter correct brand name or" +
            " \"exit\" to stop executing of the command.";
        public static string invalidSupplierNameMessage = "Please enter correct supplier name or" +
            " \"exit\" to stop executing of the command.";
        public static string invalidCategoryNameMessage = "Please enter correct category name or" +
            " \"exit\" to stop executing of the command.";
        public static string invalidPriceNameMessage = "Please enter correct price or \"exit\" " +
            "to stop executing of the command.";
        public static string invalidOnStockNameMessage = "Please enter correct number of pieces " +
            "which are on stock from the product or \"exit\" to stop executing of the command.";
        #endregion
    }
}
