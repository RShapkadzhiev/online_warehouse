﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Warehouse.Data;
using WarehouseServices;

namespace WarehouseServicesTests.UserServicesTests
{
    [TestClass]
    public class UserServices_Should
    {
        [TestMethod]
        public void CheckBase_Should()
        {
            TestsUtils.ImportRoles(nameof(CheckBase_Should));
            TestsUtils.ImportCompanies(nameof(CheckBase_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(CheckBase_Should))))
            {
                var sut = new UserServices(assertContext);
                var user1 = sut.CreateUser("Ivan", "Ivanov", "admin", "company1");
                var user2 = sut.CreateUser("Stoyan", "Stoyanov", "supplier", "company2");
                var user3 = sut.CreateUser("Milen", "Milenov", "employee", "company3");
                var users = sut.ListAllUsers();
                Assert.AreEqual(users.Count(),3);
            }
        }
        [TestMethod]
        public void AddCorrect_Should()
        {
            TestsUtils.ImportRoles(nameof(AddCorrect_Should));
            TestsUtils.ImportCompanies(nameof(AddCorrect_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(AddCorrect_Should))))
            {
                var sut = new UserServices(assertContext);
                var user = sut.CreateUser("Ivan", "Ivanov", "admin", "company1");


                Assert.AreEqual("Ivan",user.FirstName);
                Assert.AreEqual("Ivanov", user.LastName);
                Assert.AreEqual("admin", user.Role.Name);
                Assert.AreEqual("company1", user.Company.Name);
                Assert.IsTrue(user.Username.Contains('@'));

            }
        }
        [TestMethod]
        public void AlreadyExistsMessage_Should()
        {
            TestsUtils.ImportRoles(nameof(AlreadyExistsMessage_Should));
            TestsUtils.ImportCompanies(nameof(AlreadyExistsMessage_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(AlreadyExistsMessage_Should))))
            {
                var sut = new UserServices(assertContext);
                var user1 = sut.CreateUser("Ivan", "Ivanov", "admin", "company1");
            Assert.ThrowsException<ArgumentException>(() => sut.CreateUser("Ivan", "Ivanov", "admin", "company1"));
            }
        }
        [TestMethod]
        public void ModifyUserPassword_Should()
        {
            TestsUtils.ImportRoles(nameof(ModifyUserPassword_Should));
            TestsUtils.ImportCompanies(nameof(ModifyUserPassword_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(ModifyUserPassword_Should))))
            {
                var sut = new UserServices(assertContext);
                string firstName = "Ivan";
                string lastName = "Ivanov";
                string rolename = "admin";
                string companyName = "company1";
                string password = "Novaparola";
                var user = sut.CreateUser(firstName, lastName, rolename, companyName);
                var newUser = sut.ModifyPassword(password,user);
                Assert.AreEqual(newUser.Password, password);
            }
        }
        [TestMethod]
        public void RemoveUser_Should()
        {
            TestsUtils.ImportRoles(nameof(RemoveUser_Should));
            TestsUtils.ImportCompanies(nameof(RemoveUser_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(RemoveUser_Should))))
            {
                var sut = new UserServices(assertContext);
                var user1 = sut.CreateUser("Ivan", "Ivanov", "admin", "company1");
                var user2 = sut.CreateUser("Stoyan", "Stoyanov", "supplier", "company2");
                var user3 = sut.CreateUser("Milen", "Milenov", "employee", "company3");
                var user4 = sut.RemoveUser(user2);
                //user2 = sut.RemoveUser(user2);
                var users = sut.ListAllUsers();
                Assert.AreEqual(users.Count(), 2);
            }
        }
    }
}
