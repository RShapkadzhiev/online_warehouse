﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Warehouse.Data;
using WarehouseServices;

namespace WarehouseServicesTests.SupplierServicesTests
{
    [TestClass]
    public class SupplierServices_Should
    {
        [TestMethod]
        public void AddSupplierCorrect_Should()
        {
            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(AddSupplierCorrect_Should))))
            {
                var sut = new SupplierServices(assertContext);
                var user1 = sut.AddSupplier("Milka");
                var user2 = sut.AddSupplier("Lindt");
                var user3 = sut.AddSupplier("Kuma Lisa");
                
                Assert.AreEqual("Milka", user1.Name);
                Assert.AreEqual("Lindt", user2.Name);
                Assert.AreEqual("Kuma Lisa", user3.Name);
            }
        }
        [TestMethod]
        public void AlreadySupplierExistsMessage_Should()
        {
            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(AlreadySupplierExistsMessage_Should))))
            {
                var sut = new SupplierServices(assertContext);
                var user1 = sut.AddSupplier("Milka");
                var user2 = sut.AddSupplier("Milka");
                Assert.AreEqual(user2,null);
            }
        }
        [TestMethod]
        public void ListAllProductsSupplier_Should()
        {
            TestsUtils.ImportBrands(nameof(ListAllProductsSupplier_Should));
            TestsUtils.ImportCompanies(nameof(ListAllProductsSupplier_Should));
            TestsUtils.ImportSuppliers(nameof(ListAllProductsSupplier_Should));
            TestsUtils.ImportCategories(nameof(ListAllProductsSupplier_Should));

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(ListAllProductsSupplier_Should))))
            {
                var sut = new SupplierServices(assertContext);
                var sut1 = new ProductServices(assertContext);
                sut1.AddProduct("Chocolate", "Milka", "supplier1", "category1", 2, 5);
                sut1.AddProduct("Chokolate", "Kuma Lisa", "supplier1", "category1", 7, 10);
                var products = sut.GetAllProducts("supplier1");
                Assert.AreEqual(products.Count, 2);
            }
        }

        [TestMethod]
        public void ModifySupplierName_Should()
        {
            TestsUtils.ImportSuppliers(nameof(ModifySupplierName_Should));
            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(ModifySupplierName_Should))))
            {
                var sut = new SupplierServices(assertContext);
                string supplierName = "supplier1";
                string newSupplierName = "supplier20";

                var supplier = sut.GetSupplier(supplierName);
                var newSupplier = sut.ModifyName(supplier,newSupplierName);
                Assert.AreEqual(newSupplier.Name, newSupplierName);
            }
        }
        [TestMethod]
        public void RemoveSupplier_Should()
        {
            TestsUtils.ImportSuppliers(nameof(RemoveSupplier_Should));
            

            using (var assertContext = new WarehouseContext
                (TestsUtils.GetOption(nameof(RemoveSupplier_Should))))
            {
                var sut = new SupplierServices(assertContext);
                var supplier = sut.GetSupplier("supplier1");
                var supplier1 = sut.RemoveSupplier(supplier);
                Assert.AreEqual(supplier1.IsDeleted, true);
            }
        }
    }
}
