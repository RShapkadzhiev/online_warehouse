﻿using Microsoft.EntityFrameworkCore;
using Warehouse.Data;
using Warehouse.Data.Models;

namespace WarehouseServicesTests
{
    public static class TestsUtils
    {
        public static DbContextOptions GetOption(string databaseName)
        {
            return new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
        public static WarehouseContext ImportRoles(string databaseName)
        {
            var options = GetOption(databaseName);
            var arrangeContext = new WarehouseContext(options);
            arrangeContext.Roles.Add(new Role() { Name = "admin"});
            arrangeContext.Roles.Add(new Role() { Name = "user" });
            arrangeContext.Roles.Add(new Role() { Name = "supplier" });
            arrangeContext.Roles.Add(new Role() { Name = "employee" });
            arrangeContext.Roles.Add(new Role() { Name = "warehouseOwner" });
            arrangeContext.SaveChanges();
            return arrangeContext;
        }

        public static WarehouseContext ImportCategories(string databaseName)
        {
            var options = GetOption(databaseName);
            var arrangeContext = new WarehouseContext(options);
            arrangeContext.Categories.Add(new Category() { Name = "category1" });
            arrangeContext.Categories.Add(new Category() { Name = "category2" });
            arrangeContext.Categories.Add(new Category() { Name = "category3" });
            arrangeContext.Categories.Add(new Category() { Name = "category4" });
            arrangeContext.Categories.Add(new Category() { Name = "category5" });
            arrangeContext.SaveChanges();
            return arrangeContext;
        }

        public static WarehouseContext ImportBrands(string databaseName)
        {
            var options = GetOption(databaseName);
            var arrangeContext = new WarehouseContext(options);
            arrangeContext.Brands.Add(new Brand() { Name = "Milka" });
            arrangeContext.Brands.Add(new Brand() { Name = "Linds" });
            arrangeContext.Brands.Add(new Brand() { Name = "Kuma Lisa" });
            arrangeContext.Brands.Add(new Brand() { Name = "Mura" });
            arrangeContext.Brands.Add(new Brand() { Name = "Baykalche" });
            arrangeContext.SaveChanges();
            return arrangeContext;
        }

        public static WarehouseContext ImportSuppliers(string databaseName)
        {
            var options = GetOption(databaseName);
            var arrangeContext = new WarehouseContext(options);
            arrangeContext.Suppliers.Add(new Supplier() { Name = "supplier1" });
            arrangeContext.Suppliers.Add(new Supplier() { Name = "supplier2" });
            arrangeContext.Suppliers.Add(new Supplier() { Name = "supplier3" });
            arrangeContext.Suppliers.Add(new Supplier() { Name = "supplier4" });
            arrangeContext.Suppliers.Add(new Supplier() { Name = "supplier5" });
            arrangeContext.SaveChanges();
            return arrangeContext;
        }

        public static WarehouseContext ImportCompanies(string databaseName)
        {
            var options = GetOption(databaseName);
            var context = new WarehouseContext(options);
            context.Companies.Add(new Company() { Name = "company1", Address = "addres1", EIK = "111", Discount = 1 });
            context.Companies.Add(new Company() { Name = "company2", Address = "addres2", EIK = "222", Discount = 2 });
            context.Companies.Add(new Company() { Name = "company3", Address = "addres3", EIK = "333", Discount = 3 });
            context.Companies.Add(new Company() { Name = "company4", Address = "addres4", EIK = "444", Discount = 4 });
            context.Companies.Add(new Company() { Name = "company5", Address = "addres5", EIK = "555", Discount = 5 });
            context.SaveChanges();
            return context;
        }
    }
}
