﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data;
using Warehouse.Data.Models;

namespace WerehouseServicesTests.BrandServicesTests
{
    [TestClass]
    public class AddCorrect_Should
    {
        [TestMethod]
        [DataRow ("brandName1")]
        [DataRow("brandName2")]
        [DataRow("brandName3")]
        public void SuccessAdd_Shoult(string brandName)
        {
            using (var arrangeContext = new WarehouseContext(TestUtils.GetOptions(nameof(SuccessAdd_Shoult))))

            {
                arrangeContext.Brands.Add(new Brand() { Name = brandName });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new WarehouseContext(TestUtils.GetOptions(nameof(SuccessAdd_Shoult))))

            {

            }
        }
    }
}
