﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using Warehouse.Data.Configurations;
using Warehouse.Data.Models;

namespace Warehouse.Data
{
    public class WarehouseContext : DbContext
    {
        public WarehouseContext()
        {
            
        }
        public WarehouseContext(DbContextOptions options)
            :base(options)
        {

        }

        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=WarehouseSystem;Trusted_Connection=True;");
            }           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserConfigurations());
            modelBuilder.ApplyConfiguration(new SupplierConfigurations());
            modelBuilder.ApplyConfiguration(new RoleConfigurations());
            modelBuilder.ApplyConfiguration(new ProductConfigurations());
            modelBuilder.ApplyConfiguration(new PermissionConfigurations());
            modelBuilder.ApplyConfiguration(new OrderStatusConfigurations());
            modelBuilder.ApplyConfiguration(new OrderConfigurations());
            modelBuilder.ApplyConfiguration(new CompanyConfigurations());
            modelBuilder.ApplyConfiguration(new CategoryConfigurations());
            modelBuilder.ApplyConfiguration(new BrandConfigurations());

            modelBuilder.Entity<Purchase>().HasKey(p => new { p.OrderID, p.ProductID });
            modelBuilder.Entity<RolePermission>().HasKey(p => new { p.RoleID, p.PermissionID });
            modelBuilder.Entity<Category>().HasOne(x => x.Parent).WithMany(y => y.Children).HasForeignKey(f => f.UpperID);

            try
            {
                modelBuilder.Entity<Role>().HasData(GetSeedData<Role>("UserRoles"));
                modelBuilder.Entity<Permission>().HasData(GetSeedData<Permission>("Permissions"));
                modelBuilder.Entity<RolePermission>().HasData(GetSeedData<RolePermission>("RolePermission"));
                modelBuilder.Entity<Company>().HasData(GetSeedData<Company>("Company"));
                modelBuilder.Entity<User>().HasData(GetSeedData<User>("BaseUser"));
            }
            catch { }
        }

        public override int SaveChanges()
        {
            var forDeletion = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted);

            foreach (EntityEntry item in forDeletion)
            {
                item.State = EntityState.Modified;
                var baseItem = (BaseColumns)item.Entity;                
                baseItem.IsDeleted = true;
                baseItem.DeletedOn = DateTime.Now;
            }
            return base.SaveChanges();
        }

        private IEnumerable<T> GetSeedData<T>(string filename)
        {
            var jsonDir = ".\\JSON";
            string jsonFile;
            IEnumerable<T> lines = null;
            try { jsonFile = File.ReadAllText($"{jsonDir}\\{filename}.json");
                lines = JsonConvert.DeserializeObject<IEnumerable<T>>(jsonFile);
            }
            catch { }
            
            //lines = JsonConvert.DeserializeObject<IEnumerable<T>>(jsonFile);
            return lines;
        }
    }
}
