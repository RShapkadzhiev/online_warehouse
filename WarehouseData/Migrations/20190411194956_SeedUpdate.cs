﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseData.Migrations
{
    public partial class SeedUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 23 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 24 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 25 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 26 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 27 });

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "Address", "DeletedOn", "Discount", "EIK", "Name" },
                values: new object[] { 2, "ClientCompanyAddress", null, 20, "200000001", "ClientCompany" });

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "listallbrandproducts");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "createcategory");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "createcompany");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 8,
                column: "Name",
                value: "createinvoice");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "createproduct");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 12,
                column: "Name",
                value: "deleteproduct");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 13,
                column: "Name",
                value: "deletesupplier");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 14,
                column: "Name",
                value: "listallproductsofprovider");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 15,
                column: "Name",
                value: "modifyproductbrand");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 16,
                column: "Name",
                value: "modifysupplier");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 17,
                column: "Name",
                value: "showproductquantities");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 18,
                column: "Name",
                value: "help");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 19,
                column: "Name",
                value: "addproduct");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 20,
                column: "Name",
                value: "submitorder");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 21,
                column: "Name",
                value: "listorders");

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 2, null, "Client" });

            migrationBuilder.InsertData(
                table: "RolePermissions",
                columns: new[] { "RoleID", "PermissionID" },
                values: new object[,]
                {
                    { 2, 1 },
                    { 2, 2 },
                    { 2, 3 },
                    { 2, 14 },
                    { 2, 18 },
                    { 2, 19 },
                    { 2, 20 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 14 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 18 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 19 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 2, 20 });

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "login");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "createcustomer");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "assigndiscount");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 8,
                column: "Name",
                value: "changeorderstatus");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "createinvoice");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 12,
                column: "Name",
                value: "generatestockorder");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 13,
                column: "Name",
                value: "listallproductsofprovider");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 14,
                column: "Name",
                value: "listorderreport");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 15,
                column: "Name",
                value: "modifycustomeraccount");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 16,
                column: "Name",
                value: "modifyproduct");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 17,
                column: "Name",
                value: "modifysupplier");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 18,
                column: "Name",
                value: "removeproduct");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 19,
                column: "Name",
                value: "showproductquantities");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 20,
                column: "Name",
                value: "vieworderstatus");

            migrationBuilder.UpdateData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 21,
                column: "Name",
                value: "addproduct");

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 22, null, "createorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 23, null, "downloadinvoice" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 24, null, "modifyorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 25, null, "removeproduct" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 26, null, "showorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 27, null, "submitorder" });

            migrationBuilder.InsertData(
                table: "RolePermissions",
                columns: new[] { "RoleID", "PermissionID" },
                values: new object[,]
                {
                    { 1, 23 },
                    { 1, 24 },
                    { 1, 25 },
                    { 1, 26 },
                    { 1, 27 }
                });
        }
    }
}
