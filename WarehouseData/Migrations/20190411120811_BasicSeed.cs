﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseData.Migrations
{
    public partial class BasicSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "Address", "DeletedOn", "Discount", "EIK", "Name" },
                values: new object[] { 1, "Not Applicable", null, 0, "000000000", "Internal" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 26, null, "showorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 25, null, "removeproduct" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 24, null, "modifyorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 23, null, "downloadinvoice" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 22, null, "createorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 21, null, "addproduct" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 20, null, "vieworderstatus" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 19, null, "showproductquantities" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 18, null, "removeproduct" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 17, null, "modifysupplier" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 16, null, "modifyproduct" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 15, null, "modifycustomeraccount" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 27, null, "submitorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 14, null, "listorderreport" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 12, null, "generatestockorder" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 11, null, "createstatus" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 10, null, "createprovider" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 9, null, "createinvoice" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 8, null, "changeorderstatus" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 7, null, "assigndiscount" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 6, null, "createcustomer" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 5, null, "createbrand" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 4, null, "createaccount" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 3, null, "exit" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 2, null, "logout" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 1, null, "login" });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 13, null, "listallproductsofprovider" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "DeletedOn", "Name" },
                values: new object[] { 1, null, "Admin" });

            migrationBuilder.InsertData(
                table: "RolePermissions",
                columns: new[] { "RoleID", "PermissionID" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 26 },
                    { 1, 25 },
                    { 1, 24 },
                    { 1, 23 },
                    { 1, 22 },
                    { 1, 21 },
                    { 1, 20 },
                    { 1, 19 },
                    { 1, 18 },
                    { 1, 17 },
                    { 1, 16 },
                    { 1, 15 },
                    { 1, 14 },
                    { 1, 13 },
                    { 1, 12 },
                    { 1, 11 },
                    { 1, 10 },
                    { 1, 9 },
                    { 1, 8 },
                    { 1, 7 },
                    { 1, 6 },
                    { 1, 5 },
                    { 1, 4 },
                    { 1, 3 },
                    { 1, 2 },
                    { 1, 27 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CompanyID", "DeletedOn", "FirstName", "LastName", "Password", "RoleID", "Username" },
                values: new object[] { 1, 1, null, "System", "User", "admin", 1, "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 7 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 8 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 9 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 10 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 11 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 12 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 13 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 14 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 15 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 16 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 17 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 18 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 19 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 20 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 21 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 22 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 23 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 24 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 25 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 26 });

            migrationBuilder.DeleteData(
                table: "RolePermissions",
                keyColumns: new[] { "RoleID", "PermissionID" },
                keyValues: new object[] { 1, 27 });

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
