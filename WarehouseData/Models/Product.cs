﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Product : BaseColumns
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int OnStock { get; set; }
        public int BrandID { get; set; }
        public Brand Brand { get; set; }
        public int SupplierID { get; set; }
        public Supplier Supplier { get; set; }
        public int CategoryID { get; set; }
        public Category Category { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}
