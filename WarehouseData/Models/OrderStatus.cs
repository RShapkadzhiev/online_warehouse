﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class OrderStatus : BaseColumns
    {
        public string Name { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
