﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Category : BaseColumns
    {
        public string Name { get; set; }
        public int? UpperID { get; set; }
        public Category Parent { get; set; }
        public ICollection<Category> Children { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
