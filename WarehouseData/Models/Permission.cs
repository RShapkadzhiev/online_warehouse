﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Permission : BaseColumns
    {
        public string Name { get; set; }
        public ICollection<RolePermission> Roles { get; set; }
    }
}
