﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Role : BaseColumns
    {
        public string Name { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<RolePermission> Permissions { get; set; }
    }
}
