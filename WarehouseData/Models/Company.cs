﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Company : BaseColumns
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string EIK { get; set; }
        public int Discount { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
