﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Order : BaseColumns
    {
        public string Invoice { get; set; }
        public int StatusID { get; set; }
        public OrderStatus Status { get; set; }
        public int UserID { get; set; }
        public User Creator { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}
