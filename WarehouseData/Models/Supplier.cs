﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Supplier : BaseColumns
    {
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
