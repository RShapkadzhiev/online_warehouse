﻿using System;

namespace Warehouse.Data.Models
{
    public abstract class BaseColumns
    {
        public int Id { get; set; } 
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
