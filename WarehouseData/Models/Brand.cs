﻿using System.Collections.Generic;

namespace Warehouse.Data.Models
{
    public class Brand : BaseColumns
    {
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
