﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data.Models;

namespace Warehouse.Data.Configurations
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.IsDeleted).HasDefaultValue(false).IsRequired();
            builder.Property(u => u.CreatedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAdd().IsRequired();
            builder.Property(u => u.ModifiedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAddOrUpdate().IsRequired();
            //builder.Property(u => u.DeletedOn).HasDefaultValueSql("GETDATE()");

            builder.Property(u => u.FirstName).IsRequired();
            builder.Property(u => u.LastName).IsRequired();
            builder.Property(u => u.Username).IsRequired();
            builder.Property(c => c.Password).IsRequired();
            builder.Property(c => c.RoleID).IsRequired();
            builder.Property(c => c.CompanyID).IsRequired();
        }
    }    
}
