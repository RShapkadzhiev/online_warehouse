﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data.Models;

namespace Warehouse.Data.Configurations
{
    public class PermissionConfigurations : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.Property(p => p.IsDeleted).HasDefaultValue(false).IsRequired();
            builder.Property(p => p.CreatedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAdd().IsRequired();
            builder.Property(p => p.ModifiedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAddOrUpdate().IsRequired();
            //builder.Property(u => u.DeletedOn).HasDefaultValueSql("GETDATE()");

            builder.Property(p => p.Name).IsRequired();
        }
    }    
}
