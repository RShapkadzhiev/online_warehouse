﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data.Models;

namespace Warehouse.Data.Configurations
{
    public class OrderStatusConfigurations : IEntityTypeConfiguration<OrderStatus>
    {
        public void Configure(EntityTypeBuilder<OrderStatus> builder)
        {
            builder.Property(o => o.IsDeleted).HasDefaultValue(false).IsRequired();
            builder.Property(o => o.CreatedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAdd().IsRequired();
            builder.Property(o => o.ModifiedOn).HasDefaultValueSql("GETDATE()").ValueGeneratedOnAddOrUpdate().IsRequired();
            //builder.Property(u => u.DeletedOn).HasDefaultValueSql("GETDATE()");

            builder.Property(o => o.Name).IsRequired();
        }
    }    
}
