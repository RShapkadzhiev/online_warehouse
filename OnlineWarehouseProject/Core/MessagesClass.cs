﻿namespace MessagesAndValidations
{
    public static class MessagesClass
    {
        public static string introducingMessage =
            "Hello! You are using our warehouse management program." +
            "If you want to see all provided from the program services, please use \"help\""
            + "If you want to exit the program please use \"exit\"";
        public static string invalidIndexOfServiceMessage =
            "You entered invalid service index. Please enter correct index.";
        public static string invalidServiceMessage = "Invalid service.";
        public static string invalidParametersInput = "Invalid parameters input";
        public static string invalidProductName = "Invalid product name";
        public static string invalidSupplierName = "Invalid supplier name";
        public static string invalidCategoryName = "Invalid category name";
        public static string userAddedMessage = "User was created";
        public static string nameOfSupplieChangedName = "The name of supplier {0} was changed." +
            "The new name is {1}";
        public static string invalidUpperCategoryName = "Invalid general category name";
        public static string supplierDeletedMessage = "Supplier with name {0} was deleted.";
        
        public static string invalidProductNameMessage = "Invalid product name.";
        public static string invalidBrandNameMessage = "Invalid brand name.";
        public static string invalidSupplierNameMessage = "Invalid supplier name.";
        public static string invalidCategoryNameMessage = "Invalid category name.";
        public static string invalidStatusNameMessage = "Invalid status name.";
        public static string invalidPriceMessage = "Invalid price.";
        public static string invalidDiscountMessage = "Invalid discount volume.";
        public static string invalidOnStockMessage = "Invalid number of pieces.";
        public static string productDoesNotExistsMessage = "There is no registred product with name {0}.";
        public static string userDoesNotExistsMessage = "There is no registred user.";
        public static string userAlreadyExistsMessage = "This user already exists.";
        public static string userPasswordChangeMessage = "The password of the user was changed.";
        public static string productDeletedMessage = "The product was deleted.";
        public static string supplierDoesNotExistsMessage = "There is no registred supplier with name {0}.";
        public static string productAlreadyExistsMessage = "Product with name {0} already exists.";
        public static string categoryAddedMessage = "Category with name {0} was created.";
        public static string brandAddedMessage = "Brand with name {0} was created.";
        public static string supplierAddedMessage = "Supplier with name {0} was created.";
        public static string statusAddedMessage = "Status with name {0} was created.";
        public static string productAddedMessage = "Product with name {0} was created.";
        public static string productBrandModifierMessage = "The Brand of product with name {0}" +
            " was changed.The name of the brand is {1}.";
        public static string roleAddedMessage = "Role with name {0} was created.";
        public static string companyAddedMessage = "Company with name {0} was created.";
        public static string brandAlreadyExistsMessage = "Brand with name {0} already exists.";
        public static string supplierAlreadyExistsMessage = "Supplier with name {0} already exists.";
        public static string categoryAlreadyExistsMessage = "Category with name {0} already exists.";
        public static string statusAlreadyExistsMessage = "Status with name {0} already exists.";
        public static string companyAlreadyExistsMessage = "Company with name {0} already exists.";
        public static string supplierNoProductsMessage = "Supplier with name {0} has no products yet.";
        public static string brandNoProductsMessage = "Brand with name {0} has no products yet.";
        public static string noOrdersToListMessage = "There is no orders to show.";
        public static string commandDelimiter = "*************************";
    }
}
