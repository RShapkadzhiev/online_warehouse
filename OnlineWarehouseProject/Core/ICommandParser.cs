﻿using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Core
{
    public interface ICommandParser
    {
        IOrder ParseCommand(string commandName);
        string[] ParseParameters(string commandLine);
    }
}
