﻿namespace WarehouseUI.Core.IO
{
    public interface IReader
    {
        string Input();
    }
}
