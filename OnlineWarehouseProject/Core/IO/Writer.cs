﻿using System;
using System.Collections.Generic;

namespace WarehouseUI.Core.IO
{
    public class Writer : IWriter
    {
        public void PrintLine(string message)
        {
            Console.WriteLine(message);
        }
        public void PrintList(List<string> messages)
        {
            foreach (var message in messages)
            {
                Console.WriteLine(message);
            }
        }
    }
}
