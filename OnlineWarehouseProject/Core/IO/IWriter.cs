﻿using System.Collections.Generic;

namespace WarehouseUI.Core.IO
{
    public interface IWriter
    {
        void PrintLine(string message);
        void PrintList(List<string> messages);
    }
}
