﻿using System;

namespace WarehouseUI.Core.IO
{
    public class Reader : IReader

    {
        public string Input()
        {
            return Console.ReadLine();
        }
    }
}
