﻿using System.Collections.Generic;
using Warehouse.Data.Models;

namespace WarehouseUI.Core
{
    public class Session : ISession
    {
        private User activeUser = null;
        private List<string> permissions = new List<string> { "login", "help"};
        private Dictionary<int, int> shoppingCart = new Dictionary<int, int>();

        public User ActiveUser { get; set; }

        public void UserLogin(User user)
        {
            this.ActiveUser = user;
        }

        public void UserLogout()
        {
            this.ActiveUser = null;
        }

        public void AddToCart(int productId, int quantity)
        {
            if (shoppingCart.ContainsKey(productId) == true)
            {
                shoppingCart[productId] = shoppingCart[productId] + quantity;
            }
            else
            { shoppingCart.Add(productId, quantity); }
        }

        public Dictionary<int, int> RetrieveCart()
        {          
            return shoppingCart;
        }

        public void EmptyCart()
        {
            this.shoppingCart = new Dictionary<int, int>();
        }

        public void SetPermissions(List<string> permissions)
        {
            this.permissions = permissions;
        }

        public void ResetPermissions()
        {
            this.permissions = new List<string> { "login", "help" };
        }

        public List<string> GetPermissions()
        {
            return this.permissions;
        }

    }
}
