﻿using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data.Models;

namespace WarehouseUI.Core
{
    public interface ISession
    {
        User ActiveUser { get; set; }
        void UserLogin(User user);
        void UserLogout();
        void AddToCart(int product, int quantity);
        Dictionary<int, int> RetrieveCart();
        void EmptyCart();
        void SetPermissions(List<string> permissions);
        void ResetPermissions();
        List<string> GetPermissions();
    }
}
