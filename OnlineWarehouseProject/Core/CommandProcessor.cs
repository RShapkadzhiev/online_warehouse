﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Core
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly ISession sessionHandler;

        public CommandProcessor(ISession sessionHandler)
        {
            this.sessionHandler = sessionHandler;
        }

        public string ProcessCommand(IOrder command, IList<string> parameters)
        {
            string result = "";
            var commandName = command.GetType().Name.ToLower();
            var permissions = sessionHandler.GetPermissions();
            if (permissions.Contains(commandName))
            {
                result = command.Execute(parameters);
            }
            else result = $"You are not allowed to execute {commandName} command!";
            
            return this.NormalizeOutput(result);
        }

        private string NormalizeOutput(string commandOutput)
        {
            var list = commandOutput.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList().Where(x => !string.IsNullOrWhiteSpace(x));
            return string.Join("\r\n", list);
        }
    }
}
