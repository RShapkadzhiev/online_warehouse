﻿using System.Collections.Generic;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Core
{
    public interface ICommandProcessor
    {
        string ProcessCommand(IOrder command, IList<string> parameters);
    }
}
