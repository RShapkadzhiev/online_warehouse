﻿using Autofac;
using System;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Core
{
    public class CommandParser : ICommandParser
    {
        private IComponentContext componentContext;

        public CommandParser(IComponentContext context)
        {
            this.componentContext = context;
        }

        public string[] ParseParameters(string commandLine)
        {
            var lineParameters = commandLine.Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            return lineParameters;
        }

        public IOrder ParseCommand(string commandName)
        {
            var command = this.componentContext.ResolveNamed<IOrder>(commandName.ToLower());
            return command;
        }
    }
}
