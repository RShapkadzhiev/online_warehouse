﻿using WarehouseUI.Core.IO;
using System.Collections.Generic;

namespace WarehouseUI.Services.Contracts
{
    public interface ISupportedServices
    {
        List<string> GetProvidedServicesList();
        void PrintSupportedServicesList();
        void PrintSupportedServicesListByFilter(string filter);
    }
}
