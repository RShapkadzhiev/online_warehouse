﻿using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Core;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    class LogIn : IOrder
    {
        private readonly IUserService userService;
        private readonly ISession sessionHandler;

        public LogIn(IUserService userService, ISession sessionHandler)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.sessionHandler = sessionHandler ?? throw new ArgumentNullException(nameof(sessionHandler));
        }

        public string Execute(IList<string> parameters)
        {
            string username = parameters[0];
            string password = parameters[1];
            var user = userService.RetrieveUser(username);
            
            if (user == null) return "Invalid username!";
            if (user.Password != password) return "Invalid password!";
            else
            {
                sessionHandler.UserLogin(user);
                var permissions = userService.GetUserPermissions(user);
                sessionHandler.SetPermissions(permissions);
            }
            return $"User {user.FirstName} {user.LastName} logged in.";
        }        
    }
}
