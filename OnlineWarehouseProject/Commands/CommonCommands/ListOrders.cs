﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services
{
    public class ListOrders : IOrder
    {
       
        private readonly IOrderService orderService;

        public ListOrders(IOrderService orderservice)
        {
            this.orderService =
                orderservice ?? throw new ArgumentNullException(nameof(orderservice));

        }

        public string Execute(IList<string> parameters)
        {

            if (parameters.Count == 1)
            {
                var result = orderService.ListAllOrders(parameters[0]);
                if (string.IsNullOrEmpty(result))
                {
                    return MessagesClass.noOrdersToListMessage;
                }
                else
                {
                    return result;
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}

