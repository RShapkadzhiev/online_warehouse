﻿using System.Collections.Generic;
using System.Text;
using WarehouseUI.Core;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.CommonCommands
{
    public class Help : IOrder
    {        
        private readonly ISession sessionHandler;

        public Help(ISession sessionHandler)
        {
            this.sessionHandler = sessionHandler;
        }

        public string Execute(IList<string> parameters)
        {
            StringBuilder sbuilder = new StringBuilder();
            var allowedCommands = sessionHandler.GetPermissions();
            foreach (var item in allowedCommands)
            {
                sbuilder.AppendLine(item);
            }
            return sbuilder.ToString();
        }
    }
}
