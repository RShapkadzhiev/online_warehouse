﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.CommonCommands
{
    class ListAllBrandProducts : IOrder
    {
        private readonly IBrandService brandService;

        public ListAllBrandProducts(IBrandService brandService)
        {
            this.brandService = 
                brandService ?? throw new ArgumentNullException(nameof(brandService));

        }
        public string Execute(IList<string> parameters)
        {

            if (parameters.Count == 1)
            {
                StringBuilder sb = new StringBuilder();
                string brandName = parameters[0];
                List<string> products = brandService.GetAllBrandProducts(brandName);
                if (products.Count == 0)
                {
                    return string.Format(MessagesClass.brandNoProductsMessage, brandName);
                }
                else
                {
                    foreach (var product in products)
                    {
                        sb.AppendLine(product);
                    }
                    return sb.ToString();
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
