﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;
using WarehouseUI.Core;
using WarehouseUI.Core.IO;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    class LogOut : IOrder
    {
        private readonly IUserService userService;
        private readonly ISession sessionHandler;

        public LogOut(IUserService userService, ISession sessionHandler)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.sessionHandler = sessionHandler ?? throw new ArgumentNullException(nameof(sessionHandler));
        }

        public string Execute(IList<string> parameters)
        {
            sessionHandler.UserLogout();
            sessionHandler.ResetPermissions();
            sessionHandler.EmptyCart();
            return $"User logged out.";
        }        
    }
}
