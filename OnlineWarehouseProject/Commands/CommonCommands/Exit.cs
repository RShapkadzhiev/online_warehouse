﻿using System;
using System.Collections.Generic;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.CommonCommands
{
        public class Exit : IOrder
        {           
            public string Execute(IList<string> parameters)
            {
                Environment.Exit(0);
                return "Exit";
            }
        }
}
