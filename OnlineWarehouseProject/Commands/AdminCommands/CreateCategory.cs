﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using Warehouse.Data.Models;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    public class CreateCategory : IOrder
    {
        private readonly ICategoryService categoryService;

        public CreateCategory(ICategoryService categoryService)
        {
            this.categoryService =
                categoryService ?? throw new ArgumentNullException(nameof(categoryService));

        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 0 || parameters.Count >= 3)
            {
                return MessagesClass.invalidParametersInput;
            }
            Category category = null;
            string categoryName = "";
            if (parameters.Count == 1)
            {
                categoryName = parameters[0];
                category = categoryService.CreateCategory(categoryName, null);
            }
            if (parameters.Count == 2)
            {
                categoryName = parameters[0];
                string upperCategoryName = parameters[1];
                var parent = categoryService.GetCategory(upperCategoryName);
                category = categoryService.CreateCategory(categoryName, parent);
            }
            if (category != null)
            {
                return string.Format(MessagesClass.categoryAddedMessage, categoryName);
            }
            else
            {
                return string.Format(MessagesClass.categoryAlreadyExistsMessage, categoryName);
            }
   
        }
    }
}
