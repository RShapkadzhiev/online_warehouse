﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services
{
    public class CreateBrand : IOrder
    {
        private readonly IBrandService brandService;

        public CreateBrand(IBrandService brandService)
        {
            this.brandService = 
                brandService ?? throw new ArgumentNullException(nameof(brandService));
            
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                string brandName = parameters[0];
                    var brand = brandService.GetBrand(brandName);
                    if (brand != null)
                    {
                       return string.Format(MessagesClass.brandAlreadyExistsMessage, brandName);
                    }
                    else
                    {
                        var newBrand = brandService.AddNewBrand(brandName);
                        return $"Brand with name {newBrand.Name} and id {newBrand.Id} was created.";
                    }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
