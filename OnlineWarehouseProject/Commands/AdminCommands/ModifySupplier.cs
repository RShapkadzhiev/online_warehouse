﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services
{
    public class ModifySupplier : IOrder
    {
        private readonly ISupplierService supplierService;

        public ModifySupplier(ISupplierService supplierService)
        {
            this.supplierService = supplierService 
                ?? throw new ArgumentNullException(nameof(supplierService));
            
        }
        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                string supplierName = parameters[0];
                string newSupplierName = parameters[1];
                var supplier = supplierService.GetSupplier(supplierName);
                if (supplier == null)
                {
                    return (string.Format(MessagesClass.supplierDoesNotExistsMessage,
                        supplierName));
                }
                else
                {
                    var newSupplier = supplierService.ModifyName(supplier, newSupplierName);
                    return string.Format(MessagesClass.nameOfSupplieChangedName,
                        supplierName, newSupplierName);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
