﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    public class ModifyUserPassword : IOrder
    {
        private readonly IUserService userService;

        public ModifyUserPassword(IUserService userService)
        {
            this.userService = userService ??
                throw new ArgumentNullException(nameof(userService));
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 4)
            {
                string firstName = parameters[0];
                string lastName = parameters[1];
                string companyName = parameters[2];
                string newPassword = parameters[3];
                var user = userService.GetUser(firstName, lastName, companyName);
                if (user == null)
                {
                    return MessagesClass.userDoesNotExistsMessage;
                }
                else
                {
                    user = userService.ModifyPassword(newPassword, user);
                    return MessagesClass.userPasswordChangeMessage;
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
