﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseServices.Contracts;
using WarehouseUI.Core.IO;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    class ListAllProductsOfProvider : IOrder
    {
        private readonly ISupplierService supplierService;

        public ListAllProductsOfProvider(ISupplierService supplierService)
        {
            this.supplierService = supplierService ??
                throw new ArgumentNullException(nameof(supplierService));

        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {

                List<string> products = supplierService.GetAllProducts(parameters[0]);
                if (products == null)
                {
                    return string.Format(MessagesClass.supplierNoProductsMessage, parameters[0]);
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var product in products)
                    {
                        sb.AppendLine(product);
                    }
                    return sb.ToString();

                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
