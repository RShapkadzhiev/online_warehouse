﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services.AdminServices
{
    public class CreateProduct : IOrder
    {
        private readonly IProductService productService;

        public CreateProduct(IProductService productService)
        {
            this.productService = productService ??
                throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 6)
            {
                string productName = parameters[0];
                string brandName = parameters[1];
                string supplierName = parameters[2];
                string categoryName = parameters[3];
                var price = ValidationClass.isPriceCorrect(parameters[4]);
                var onStock = ValidationClass.isonStockCorrect(parameters[5]);
                var product = productService.AddProduct(productName, brandName, supplierName,
                    categoryName, price, onStock);
                if (product == null)
                {
                    return MessagesClass.productAlreadyExistsMessage;
                }
                else
                {
                    return string.Format(MessagesClass.productAddedMessage, productName);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
