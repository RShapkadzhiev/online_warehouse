﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    class CreateStatus : IOrder
    {
        private readonly IOrderService orderService;

        public CreateStatus(IOrderService orderService)
        {
            this.orderService = orderService ??
                throw new ArgumentNullException(nameof(orderService));
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                string statusName = parameters[0];
                var status = orderService.CreateStatus(statusName);
                if (status != null)
                {
                    return string.Format(MessagesClass.statusAddedMessage, statusName);
                }
                else
                {
                    return string.Format(MessagesClass.statusAlreadyExistsMessage, statusName);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}