﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services
{
    public class CreateProvider : IOrder
    {
        private readonly ISupplierService supplierService;


        public CreateProvider(ISupplierService supplierService)
        {
            this.supplierService = supplierService 
                ?? throw new ArgumentNullException(nameof(supplierService));
        }

        public string Execute(IList<string> parameters)
        {
            string supplierName = parameters[0];
            var supplier = supplierService.AddSupplier(supplierName);
            if (supplier != null)
            {
                return $"Supplier with name {supplier.Name} was created.";
            }
            else
            {
                return string.Format(MessagesClass.supplierAddedMessage, supplier.Name);
            }
        }
    }
}
