﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    public class CreateCompany : IOrder
    {
        private readonly ICompanyService companyService;

        public CreateCompany(ICompanyService compnayService)
        {
            this.companyService =
                compnayService ?? throw new ArgumentNullException(nameof(compnayService));
        }
        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 4)
            {
                string companyName = parameters[0];
                string companyAddress = parameters[1];
                string eik = parameters[2];
                var discountAsNumber = ValidationClass.isDiscountCorrect(parameters[3]);
                if (discountAsNumber > 0)
                {
                    var company = companyService.CreateCompany(companyName, companyAddress,
                        eik, discountAsNumber);
                    if (company != null)
                    {
                        return string.Format(MessagesClass.companyAddedMessage, companyName);
                    }
                    else
                    {
                        return string.Format(MessagesClass.companyAlreadyExistsMessage, companyName);
                    }
                }
                else
                {
                    return MessagesClass.invalidDiscountMessage;
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}

