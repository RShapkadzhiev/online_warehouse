﻿using WarehouseUI.Services.Contracts;
using System;
using WarehouseServices.Contracts;
using WarehouseUI.Core.IO;
using MessagesAndValidations;
using System.Collections.Generic;

namespace WarehouseUI.Services.Services
{
    public class DeleteSupplier : IOrder
    {
        private readonly ISupplierService supplierService;

        public DeleteSupplier(ISupplierService supplierService)
        {
            this.supplierService = supplierService ??
                throw new ArgumentNullException(nameof(supplierService));
            
        }
        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                string supplierName = parameters[0];
                var supplier = supplierService.GetSupplier(supplierName);
                if (supplier == null)
                {
                    return string.Format(MessagesClass.supplierDoesNotExistsMessage, supplierName);
                }
                else
                {
                    supplierService.RemoveSupplier(supplier);
                    return string.Format(MessagesClass.supplierDeletedMessage, supplierName);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
            
        }
    }
}