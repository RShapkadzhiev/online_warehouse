﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    class CreateAccount : IOrder
    {
        private readonly IUserService userService;

        public CreateAccount(IUserService userService)
        {
            this.userService = userService ??
                throw new ArgumentNullException(nameof(userService));
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 4)
            {
                string firstName = parameters[0];
                string lastName = parameters[1];
                string roleName = parameters[2];
                string companyName = parameters[3];
                var ifUserExists = userService.GetUser(firstName, lastName, companyName);
                if (ifUserExists == null)
                {
                    var user = userService.CreateUser(firstName, lastName, roleName, companyName);
                    return MessagesClass.userAddedMessage;
                }
                else
                {
                    return MessagesClass.userAlreadyExistsMessage;
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}
