﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services
{
    public class ShowProductQuantities : IOrder
    {
        private readonly IProductService productService;

        public ShowProductQuantities(IProductService productService)
        {
            this.productService = productService ??
                throw new ArgumentNullException(nameof(productService));

        }
        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                var quantity = productService.GetProductQuantity(parameters[0]);
                if (quantity >= 0)
                {
                    return quantity.ToString();
                }
                else
                {
                    return string.Format(MessagesClass.productDoesNotExistsMessage, parameters[0]);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }
        }
    }
}

