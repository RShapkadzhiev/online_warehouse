﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Services.Services.AdminServices
{
    public class DeleteProduct : IOrder
    {
        private readonly IProductService productService;

        public DeleteProduct(IProductService productService)
        {
            this.productService = productService ??
                throw new ArgumentNullException(nameof(productService));
            
        }
        public string Execute(IList<string> parameters)
        {
            if (parameters.Count == 1)
            {
                string productName = parameters[0];
                var product = productService.GetProduct(productName);
                if (product == null)
                {
                    return string.Format(MessagesClass.productDoesNotExistsMessage, productName);
                }
                else
                {
                    var removedProduct = productService.RemoveProduct(product);
                    return string.Format(MessagesClass.productDeletedMessage);
                }
            }
            else
            {
                return MessagesClass.invalidParametersInput;
            }

        }

    }
}
