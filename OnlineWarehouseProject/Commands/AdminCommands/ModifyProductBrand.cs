﻿using MessagesAndValidations;
using System;
using System.Collections.Generic;
using WarehouseServices.Contracts;
using WarehouseUI.Services.Contracts;

namespace WarehouseUI.Commands.AdminCommands
{
    public class ModifyProductBrand : IOrder
    {
        private readonly IProductService productService;

        public ModifyProductBrand(IProductService productService)
        {
            this.productService = productService ??
                throw new ArgumentNullException(nameof(productService));

        }
        public string Execute(IList<string> parameters)
        {
            string brandName = parameters[1];
            var product = productService.GetProduct(parameters[0]);
            if (product == null)
            {
                return string.Format(MessagesClass.productDoesNotExistsMessage, parameters[0]);
            }
            else
            {
                var newProduct = productService.ChangeBrand(product, brandName);
                return string.Format(MessagesClass.productBrandModifierMessage, newProduct.Name, brandName);
            }
        }
    }
}
