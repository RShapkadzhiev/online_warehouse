﻿using WarehouseUI.Services.Contracts;
using System;
using System.Collections.Generic;
using WarehouseUI.Core;
using WarehouseServices.Contracts;
using MessagesAndValidations;
using Warehouse.Data.Models;

namespace WarehouseUI.Services.Services.ClientsServices
{
    public class AddProduct : IOrder
    {
        private readonly ISession sessionHandler;
        private readonly IProductService productService;

        public AddProduct(ISession sessionHandler, IProductService productService)
        {
            this.sessionHandler = sessionHandler;
            this.productService = productService;
        }

        public string Execute(IList<string> parameters)
        {
            if (parameters.Count < 2) return MessagesClass.invalidParametersInput;
            var productName = parameters[0];
            var quantity = int.Parse(parameters[1]);
            var product = productService.GetProduct(productName);
            if (product == null) return MessagesClass.productDoesNotExistsMessage;
            sessionHandler.AddToCart(product.Id, quantity);
            return $"{product.Name} added to cart.";
        }
    }
}
