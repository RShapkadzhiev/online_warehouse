﻿using WarehouseUI.Services.Contracts;
using System;
using System.Collections.Generic;
using WarehouseUI.Core;
using WarehouseServices.Contracts;

namespace WarehouseUI.Services.Services
{
    public class SubmitOrder : IOrder
    {
        private readonly ISession sessionHandler;
        private readonly IOrderService orderService;

        public SubmitOrder(ISession sessionHandler, IOrderService orderService)
        {
            this.sessionHandler = sessionHandler;
            this.orderService = orderService;
        }

        public string Execute(IList<string> parameters)
        {
            var purchases = sessionHandler.RetrieveCart();
            var user = sessionHandler.ActiveUser;
            var order = orderService.CreateOrder("submitted", user.Id);
            foreach (var item in purchases)
            {
                orderService.AddPurchase(order.Id, item.Key, item.Value);
            }
            sessionHandler.EmptyCart();
            return $"Order {order.Id} submitted.";
        }
    }
}
