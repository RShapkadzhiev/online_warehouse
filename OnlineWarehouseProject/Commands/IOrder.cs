﻿using System.Collections.Generic;

namespace WarehouseUI.Services.Contracts
{
    public interface IOrder
    {
        string Execute(IList<string> parameters);
    }
}
