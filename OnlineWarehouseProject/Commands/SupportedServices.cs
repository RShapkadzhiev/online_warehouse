﻿using WarehouseUI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WarehouseUI.Core.IO;

namespace WarehouseUI.Services.Services
{
    public class SupportedServices : ISupportedServices
    {

        private readonly IWriter writer;

        public SupportedServices(IWriter writer)
        {
            this.writer = writer;
        }

        public List<string> GetProvidedServicesList()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var services = assembly.DefinedTypes.Where(typeinfo =>
            typeinfo.ImplementedInterfaces.Contains(typeof(IOrder))).ToList();
            List<string> servicesNames = new List<string>();
            foreach (var service in services)
            {
                servicesNames.Add(service.Name);
            }
            return servicesNames;
        }
        public void PrintSupportedServicesList()
        {
            List<string> supportedServices = GetProvidedServicesList();
            int servicesIndex = 0;
            writer.PrintLine("The program provides the following services:");
            foreach (var service in supportedServices)
            {
                servicesIndex++;
                writer.PrintLine($" ({servicesIndex}) - {service}");
            }
            if (servicesIndex == 0)
            {
                writer.PrintLine("Currently the program does not provide any services." +
                    " Please contact us for technical support.");
            }
            
        }
     
        public void PrintSupportedServicesListByFilter(string filter)
        {
            List<string> supportedServices = GetProvidedServicesList();
            int servicesIndex = 0;
            bool isThereSuchServices = false;
            writer.PrintLine($"The program provides the following services filtered by the word \"{filter}\":");
            foreach (var service in supportedServices)
            {
                servicesIndex++;
                if (service.ToLower().Contains(filter.ToLower()))
                {
                    isThereSuchServices = true;
                    writer.PrintLine($" ({servicesIndex}) - {service}");
                }
                
            }
            if (!isThereSuchServices)
            {
                writer.PrintLine($"Currently there is no provided services, which contain the word \"{filter}\"");
            }
        }
    }
}
