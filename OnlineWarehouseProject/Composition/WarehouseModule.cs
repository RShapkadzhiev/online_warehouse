﻿using Autofac;
using System.Linq;
using System.Reflection;
using Warehouse.Data;
using WarehouseServices;
using WarehouseServices.Contracts;
using WarehouseUI.Core.IO;
using WarehouseUI.Services.Contracts;
using WarehouseUI.Services.Services;

namespace WarehouseUI.Core
{
    public class WarehouseModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WarehouseContext>().AsSelf()
                .InstancePerLifetimeScope();
            builder.RegisterType<CategoryServices>().As<ICategoryService>();
            builder.RegisterType<CompanyServices>().As<ICompanyService>();
            builder.RegisterType<OrderServices>().As<IOrderService>();
            builder.RegisterType<ProductServices>().As<IProductService>();
            builder.RegisterType<PermissionServices>().As<IPermissionService>();
            builder.RegisterType<BrandServices>().As<IBrandService>();
            builder.RegisterType<SupplierServices>().As<ISupplierService>();
            builder.RegisterType<UserServices>().As<IUserService>();
            builder.RegisterType<Engine>().AsSelf();
            builder.RegisterType<Reader>().As<IReader>().SingleInstance();
            builder.RegisterType<Writer>().As<IWriter>().SingleInstance();
            builder.RegisterType<CommandParser>().As<ICommandParser>().SingleInstance();
            builder.RegisterType<CommandProcessor>().As<ICommandProcessor>();
            builder.RegisterType<SupportedServices>().As<ISupportedServices>();
            builder.RegisterType<Session>().As<ISession>().SingleInstance();
            RegisterCommands(builder);
        }

        private void RegisterCommands(ContainerBuilder builder)
        {
            var commands = Assembly.GetExecutingAssembly()
                .DefinedTypes
                .Where(t => t.ImplementedInterfaces.Contains(typeof(IOrder)));

            foreach (var command in commands)
            {
                builder.RegisterType(command.AsType())
                    .Named<IOrder>(command.Name.ToLower().Replace("command", ""));
            }
        }
    }
}