﻿using MessagesAndValidations;
using System.Linq;
using WarehouseUI.Core.IO;

namespace WarehouseUI.Core
{
    public class Engine 
    {
        private readonly IWriter writer;
        private readonly IReader reader;
        private readonly ICommandParser parser;
        private readonly ICommandProcessor processor;

        public Engine(IWriter writer, IReader reader, ICommandParser parser, ICommandProcessor processor)
        {
            this.reader = reader;
            this.writer = writer;
            this.parser = parser;
            this.processor = processor;
        }

        public void Start()
        {
            writer.PrintLine(MessagesClass.introducingMessage);
            string commandLine = null;
            while (true)
            {
                commandLine = this.reader.Input();             
                var parameters = this.parser.ParseParameters(commandLine);
                var command = this.parser.ParseCommand(parameters[0]);
                var result = this.processor.ProcessCommand(command, parameters.Skip(1).ToList());
                writer.PrintLine(result);
                writer.PrintLine(MessagesClass.commandDelimiter);
            }

        }

    }
}