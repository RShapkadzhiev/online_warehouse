﻿using MessagesAndValidations;
using System;

namespace WarehouseUI
{
    public static class ValidationClass
    {
        public static bool isNameCorrect(string name)
        {
            bool isCorrect = true;
            if (string.IsNullOrEmpty(name))
            {
                isCorrect = false;
            }
            return isCorrect;
        }

        public static decimal isPriceCorrect(string number)
        {
            decimal decim;
            var isCorrect = decimal.TryParse(number, out decim);
            if (decim <= 0 || isCorrect == false)
            {
                throw new ArgumentException(MessagesClass.invalidPriceMessage);
            }
            else
            {
                return decim;
            }
        }

        public static int isonStockCorrect(string number)
        {
            int integer;
            var isCorrect = int.TryParse(number, out integer);
            if (integer <= 0 || isCorrect == false)
            {
                throw new ArgumentException(MessagesClass.invalidOnStockMessage);
            }
            else
            {
               return integer;
            }
        }

        public static int isDiscountCorrect(string number)
        {
            int integer;
            var isCorrect = int.TryParse(number, out integer);
            if (isCorrect)
            {
                integer = int.Parse(number);
                if (integer > 0)
                {
                    return integer;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
    }
}
